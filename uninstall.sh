#/bin/zsh

# Default settings
INIT_DIR="${INIT_DIR:-$HOME/.init}"
INITRC="${INIT_DIR}/.initrc"

# # 卸载 omz
# echo "卸载 oh my zsh"
# /bin/sh -c "REPO='mirrors/ohmyzsh'; REMOTE='https://gitee.com/mirrors/ohmyzsh.git' ;$(curl -fsSL https://gitee.com/mirrors/oh-my-zsh/raw/master/tools/uninstall.sh)"

# # 卸载 brew
# echo "卸载 brew"
# brew cleanup
# /bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/HomebrewUninstall.sh)"

echo "移除初始化目录"
mv $INIT_DIR $HOME/Desktop/init

echo "文件夹已备份至桌面: ${HOME}/Desktop/init"
echo
echo "提示： 按下 command + shift + . 可以查看隐藏文件"
