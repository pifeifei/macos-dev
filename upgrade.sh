#/bin/zsh

CURRENT_DIR=`pwd`

echo "update brew"
brew update && brew outdated && brew upgrade && brew cleanup


# update oh my zsh
echo "update oh my zsh"
cd $ZSH
./tools/upgrade.sh

cd $CURRENT_DIR