# MacOS 初始化

## 1. 说明

> 更好电脑设备时配置新环境，需要花费大量的时间，所以尝试创建一个自动化脚本，改脚本针对程序员使用，因为我使用的是 `PHP`，所以更倾向 `PHP`，如果我有用到其他软件也会加上，同时也欢迎大家提交`pr`。

## 2. 使用方法

```shell
# 运行如下代码即可安装。

/bin/zsh -c "$(curl -fsSL https://gitee.com/pifeifei/macos-init/raw/master/init.sh)" all


# 参数说明（顺序无关
#   all: 安装所有软件（不含付费软件）
#   - speed：git 快速下载（不建议使用，*使用后不能安装历史版本的软件*）
#   php：php，composer, phpstorm(付费)
#   js：nodejs, vscode, webstorm(付费)
#   db: another-redis-desktop-manager, mysqlworkbench, DataGrip(付费)
#   java: 等待补充
#   tencent: 腾讯软件，tencent-lemon qq wechat qqmusic
#   charge: 安装付费软件
# 个性化安装：
#/bin/zsh -c "$(curl -fsSL https://gitee.com/pifeifei/macos-init/raw/master/init.sh)" php js db tencent


#卸载(未实现)
# 删除 $HOME/.init/ 、 homebrew、ohmyzsh 即可
```

## 3. 安装的软件

> 默认只安装免费版本的软件，安装付费软件请使用 `init.sh all`。
> 如果已存在，将会重新安装（brew 是重新安装）、重新配置。

### 3.1 默认安装的软件

* brew
* oh-my-zsh 及插件
  * git-open
  * autojump
  * zsh-autosuggestions
  * zsh-completions
  * zsh-syntax-highlighting
* bat
* wget
* get-open: 从终端快速打开 git 仓库
* 4k-video-downloader: 4k视频下载器（免费版基本够用了）
* dash（收费）未安装
* docker desktop
* free-download-manager: 免费开源下载软件，*下载慢，未安装*
* google-chrome: 浏览器
* iina： 视频播放器
* iterm2：终端
* lulu: 开源的 mac 应用防火墙软件
* microsoft-remote-desktop： windows 远程桌面管理
* postman： 老牌 API 接口调试工具
* sourcetree： git UI
* switchhosts： hosts 管理工具
* vlc： 开源的视频播放器
* utools： 效率工具集
* youtube-dl： youtube 下载

### 3.2 javascript

* nodejs
* visual-studio-code
* webstorm (收费)

### 3.3 PHP

* php: 8.1, 8.0, 7.4
* composer: v2.x
* phpStorm(收费)

### 3.4 数据库

> 不安装数据库，仅安装数据库管理软件

* another-redis-desktop-manager
* mysqlworkbench
* DataGrip(收费)

### 3.5 腾讯全家桶

* 微信
* qq
* qq 音乐
* 腾讯柠檬：Mac 清理工具

### 3.2 默认配置

* git


### 3.x 其他软件列表

* adguard for safari: 广告屏蔽
* 编辑器： VSCode, Atom,Vim , sublime text, vim, Emacs
* 终端工具：Iterm2, oh-my-zsh, brew, tmux, vim, autojump, fzf
* Alfread：快速查找软件，文件等
* Dash：文档
* 浏览器：chrome
* magnet： 分屏
* karabiner： 键位映射
* neovim：定制编辑器
* clashX： 你懂的

## 4. 其他说明

* 安装目录： ${HOME}/.init/
* 软件无法运行：点开 系统偏好设置 -> 安全性与隐私 -> 通用 -> 允许从以下位置下载 app -> 找到对应软件 -> 点击右侧的允许运行
